/*
 * Copyright (c) 2020-2021. the original authors and DEPSEA.ORG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.depsea.log.handler.impl;

import lombok.extern.slf4j.Slf4j;
import org.depsea.log.handler.RequestPointHandler;
import org.depsea.log.point.RequestPoint;

/**
 * 打印请求日志
 *
 * @author jaune
 * @since 1.0.0
 */
@Slf4j
public class RequestLogHandler implements RequestPointHandler {

    @Override
    public void handle(RequestPoint requestPoint) {
        log.info("Request Info: {} {} {}", requestPoint.getSchema(), requestPoint.getRequestMethod(), requestPoint.getRequestUri());
        log.info("Controller & Method: {}#{}", requestPoint.getClazz(), requestPoint.getMethodName());
        log.info("Request Parameters: {}", requestPoint.getRequestParameterMap());
        log.info("Method Parameters: {}", requestPoint.getMethodParameterMap());
        if (requestPoint.isError()) {
            log.warn("Request Error: {}", requestPoint.getErrorMessage());
        } else {
            log.info("Return Value：{}", requestPoint.getReturnValue());
        }
        log.info("Time Consuming: {}", requestPoint.getTimeConsuming());
    }
}
