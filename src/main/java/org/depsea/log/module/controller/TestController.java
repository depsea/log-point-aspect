/*
 * Copyright (c) 2020-2021. the original authors and DEPSEA.ORG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.depsea.log.module.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.depsea.log.module.service.TestService;
import org.depsea.log.utils.TestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jaune
 * @since 1.0.0
 */
@Api(value = "测试")
@Slf4j
@RestController
@RequestMapping("/api/v1/test")
public class TestController {

    private final TestService testService;
    private final TestUtils testUtils;

    public TestController(TestService testService, TestUtils testUtils) {
        this.testService = testService;
        this.testUtils = testUtils;
    }

    @ApiOperation("hello")
    @GetMapping
    public String test(String name) {
        log.info("name: {}", name);
        this.testService.test();
        testUtils.test();
        return "OK";
    }
}
