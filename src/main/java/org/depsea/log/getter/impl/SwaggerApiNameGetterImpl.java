/*
 * Copyright (c) 2020-2021. the original authors and DEPSEA.ORG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.depsea.log.getter.impl;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.depsea.log.getter.ApiNameGetter;

import java.lang.reflect.Method;
import java.util.concurrent.ExecutionException;

/**
 * 基于Swagger2获取模块名称及接口名称
 *
 * @author jaune
 * @since 1.0.0
 */
@Slf4j
public class SwaggerApiNameGetterImpl implements ApiNameGetter {

    private final LoadingCache<Method, String> operationNameCache;
    private final LoadingCache<Class<?>, String> moduleNameCache;

    public SwaggerApiNameGetterImpl(CacheLoader<Method, String> operationNameCacheLoader, CacheLoader<Class<?>, String> moduleNameCacheLoader) {
        operationNameCache = CacheBuilder.newBuilder()
                .maximumSize(2000)
                .build(operationNameCacheLoader);

        moduleNameCache = CacheBuilder.newBuilder()
                .maximumSize(500)
                .build(moduleNameCacheLoader);
    }

    @Override
    public String getOperationName(Method method) {
        try {
            return this.operationNameCache.get(method);
        } catch (ExecutionException e) {
            if (log.isDebugEnabled()) {
                log.warn(e.getMessage(), e);
            } else {
                log.warn(e.getMessage());
            }
            return "";
        }
    }

    @Override
    public String getModuleName(Class<?> targetClazz) {
        try {
            return this.moduleNameCache.get(targetClazz);
        } catch (ExecutionException e) {
            if (log.isDebugEnabled()) {
                log.warn(e.getMessage(), e);
            } else {
                log.warn(e.getMessage());
            }
            return "";
        }
    }
}
