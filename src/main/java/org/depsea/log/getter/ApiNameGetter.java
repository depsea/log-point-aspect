/*
 * Copyright (c) 2020-2021. the original authors and DEPSEA.ORG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.depsea.log.getter;

import java.lang.reflect.Method;
import java.util.concurrent.ExecutionException;

/**
 * Get api name and module name
 *
 * @author jaune
 * @since 1.0.0
 */
public interface ApiNameGetter {

    /**
     * Get api name from method's annotation. Such as swagger's @ApiOperation
     *
     * @param method Controller Method
     * @return api name
     */
    String getOperationName(Method method);

    /**
     * Get module name from Controller class's annotation. Such as swagger's @Api
     * @param targetClazz Controller Class
     * @return module name
     */
    String getModuleName(Class<?> targetClazz);
}
