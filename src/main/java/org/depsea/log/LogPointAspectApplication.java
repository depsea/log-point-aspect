package org.depsea.log;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogPointAspectApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogPointAspectApplication.class, args);
    }

}
