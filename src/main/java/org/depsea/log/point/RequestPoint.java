/*
 * Copyright (c) 2020-2021. the original authors and DEPSEA.ORG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.depsea.log.point;

import lombok.Data;

import java.util.Date;

/**
 * 请求信息
 *
 * @author jaune
 * @since 1.0.0
 */
@Data
public class RequestPoint {

    /**
     * 应用名称
     */
    private String applicationName;

    /**
     * 追踪链路Id
     */
    private String traceId;

    /**
     * 节点id
     */
    private String spanId;

    /**
     * 上级节点ID
     */
    private String parentId;

    /**
     * 模块名称 -- swagger注解中获取
     */
    private String moduleName;

    /**
     * 接口名称 -- swagger 注解中获取
     */
    private String apiName;

    /**
     * http schema, HTTP or HTTPS
     */
    private String schema;

    /**
     * 请求方法
     */
    private String requestMethod;

    /**
     * 请求地址
     */
    private String requestUri;

    /**
     * 本地服务器IP
     */
    private String serverLocalIp;

    /**
     * 接口类的名称
     */
    private String clazz;

    /**
     * 接口方法的名称
     */
    private String methodName;

    /**
     * 请求参数，本来是map但是为了能够在存储到ES中时，不因数据类型和字段发生变更而导致一些错误，所以使用字符串
     */
    private String requestParameterMap;

    /**
     * Spring将请求参数解析后注入到方法参数后的数据
     */
    private String methodParameterMap;

    /**
     * 接口返回的数据
     */
    private String returnValue;

    /**
     * 耗时
     */
    private long timeConsuming;

    /**
     * 是否发生错误
     */
    private boolean error;

    /**
     * 错误消息
     */
    private String errorMessage;

    /**
     * 异常类
     */
    private String exceptionName;

    /**
     * 异常栈
     */
    private String exceptionStack;

    /**
     * 请求完成时间
     */
    private Date timestamp;

    /**
     * Response 状态
     */
    private int responseStatus;

    /**
     * 请求头
     */
    private String requestHeaders;

    /**
     * 应答头
     */
    private String responseHeaders;

    /**
     * cookie 数据
     */
    private String cookies;

    /**
     * 请求接口的用户ID
     */
    private String userId;

    /**
     * 请求接口的用户姓名
     */
    private String name;

}
