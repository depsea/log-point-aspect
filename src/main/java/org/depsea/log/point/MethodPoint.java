/*
 * Copyright (c) 2020-2021. the original authors and DEPSEA.ORG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.depsea.log.point;

import java.util.ArrayList;

/**
 * @author jaune
 * @since 1.0.0
 */
public class MethodPoint {

    private MethodContext root;

    private MethodContext current;

    public void setRoot(MethodContext root) {
        this.root = root;
        this.current = root;
    }

    public void up() {
        this.current =current.getParent();
    }

    public MethodContext getCurrent() {
        return current;
    }

    public void addChildren(MethodContext methodContext) {
        if (current.getChildren() == null) {
            current.setChildren(new ArrayList<>());
        }
        current.getChildren().add(methodContext);
        methodContext.setParent(current);
        current = methodContext;

    }
}
