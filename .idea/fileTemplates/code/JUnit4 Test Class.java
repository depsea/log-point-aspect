#parse("File Header.java")
#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end
import static org.assertj.core.api.Assertions.*;

/**
 * Test for {@link ${CLASS_NAME}}
 */
public class ${NAME} {
  ${BODY}
}